package com.example.juanp.tourappguide.Objects;

import android.os.Parcel;
import android.os.Parcelable;

public class Items implements Parcelable{

    //Declaration of the variables
    private String name;
    private String location;
    private String information;
    private String coordinates;
    private int imgItem;


    public Items(String name, String location, String information, String coordinates, int imgItem) {
        this.name = name;
        this.location = location;
        this.information = information;
        this.coordinates = coordinates;
        this.imgItem = imgItem;
    }


    protected Items(Parcel in) {
        name = in.readString();
        location = in.readString();
        information = in.readString();
        coordinates = in.readString();
        imgItem = in.readInt();
    }

    public static final Creator<Items> CREATOR = new Creator<Items>() {
        @Override
        public Items createFromParcel(Parcel in) {
            return new Items(in);
        }

        @Override
        public Items[] newArray(int size) {
            return new Items[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(location);
        dest.writeString(information);
        dest.writeString(coordinates);
        dest.writeInt(imgItem);
    }

    //Getters
    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getInformation() {
        return information;
    }

    public int getImgItem() {
        return imgItem;
    }

    public String getCoordinates() {
        return coordinates;
    }
}
