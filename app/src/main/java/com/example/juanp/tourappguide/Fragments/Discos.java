package com.example.juanp.tourappguide.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.juanp.tourappguide.Adapter.CustomAdapter;
import com.example.juanp.tourappguide.Objects.Items;
import com.example.juanp.tourappguide.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Discos extends Fragment {

    //Declaration of the arraylist
    private ArrayList<Items> listDiscos;
    //Declaration of the recyclerview
    private RecyclerView recyclerDiscos;

    public Discos() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Create the view and inflate it
        View view = inflater.inflate(R.layout.fragment_contents, container, false);

        //Method to add the items to the arraylist
        listPubs();
        //Method to create the recyclerview
        buildRecyclerView(view);

        return view;
    }

    //List method
    private void listPubs() {

        listDiscos = new ArrayList<>();

        //Create items
        listDiscos.add(new Items(getString(R.string.disco_pelicano_name), getString(R.string.disco_pelicano_location), getString(R.string.disco_pelicano_info), getString(R.string.disco_pelicano_coordinates), R.drawable.disco_pelicano));
        listDiscos.add(new Items(getString(R.string.disco_playa_name), getString(R.string.disco_playa_location), getString(R.string.disco_playa_info), getString(R.string.disco_playa_coordinates), R.drawable.disco_playa));
        listDiscos.add(new Items(getString(R.string.disco_moom_name), getString(R.string.disco_moom_location), getString(R.string.disco_moom_info), getString(R.string.disco_moom_coordinates), R.drawable.disco_moom));
    }

    //Build recyclerview method
    private void buildRecyclerView(View view) {

        //Set the view id and the layout manager
        recyclerDiscos = view.findViewById(R.id.recyclerContents);
        recyclerDiscos.setLayoutManager(new LinearLayoutManager(getContext()));

        //Create and set the adapter
        CustomAdapter PubsAdapter = new CustomAdapter(listDiscos);
        recyclerDiscos.setAdapter(PubsAdapter);

        //Clicklistener
        PubsAdapter.setOnItemClickListener(new CustomAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                //Intent to open google maps location
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + listDiscos.get(position).getCoordinates());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
    }
}
