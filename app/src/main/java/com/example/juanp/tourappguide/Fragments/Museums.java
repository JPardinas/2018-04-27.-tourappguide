package com.example.juanp.tourappguide.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.juanp.tourappguide.Adapter.CustomAdapter;
import com.example.juanp.tourappguide.Objects.Items;
import com.example.juanp.tourappguide.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Museums extends Fragment {

    //Declaration of the arraylist with the museums
    private ArrayList<Items> listMuseums;
    //Declaration of the recyclerview
    private RecyclerView recyclerMuseums;

    public Museums() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Create the view and inflate it
        View view = inflater.inflate(R.layout.fragment_contents, container, false);

        //Method to add the museums to the arraylist
        listMuseums();
        //Method to create the recyclerview
        buildRecyclerView(view);

        return view;
    }

    //List museums method
    private void listMuseums() {

        listMuseums = new ArrayList<>();

        //Create museums items
        listMuseums.add(new Items(getString(R.string.museums_domus_name), getString(R.string.museums_domus_location), getString(R.string.museums_domus_info), getString(R.string.museums_domus_coordinates), R.drawable.museum_domus));
        listMuseums.add(new Items(getString(R.string.museums_casaciencias_name), getString(R.string.museums_casaciencias_location), getString(R.string.museums_casaciencias_info), getString(R.string.museums_casaciencias_coordinates), R.drawable.museums_casaciencias));
        listMuseums.add(new Items(getString(R.string.museums_finisterrae_name), getString(R.string.museums_finisterrae_location), getString(R.string.museums_finisterrae_info), getString(R.string.museums_finisterrae_coordinates), R.drawable.museums_finisterrae));
        listMuseums.add(new Items(getString(R.string.museums_military_name), getString(R.string.museums_military_location), getString(R.string.museums_military_info), getString(R.string.museums_military_coordinates), R.drawable.museums_military));
    }

    //Build recyclerview method
    private void buildRecyclerView(View view) {

        //Set the view id and the layout manager
        recyclerMuseums = view.findViewById(R.id.recyclerContents);
        recyclerMuseums.setLayoutManager(new LinearLayoutManager(getContext()));

        //Create and set the adapter
        CustomAdapter museumsAdapter = new CustomAdapter(listMuseums);
        recyclerMuseums.setAdapter(museumsAdapter);

        //Clicklistener
        museumsAdapter.setOnItemClickListener(new CustomAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                //Intent to open google maps location
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + listMuseums.get(position).getCoordinates());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
    }
}
