package com.example.juanp.tourappguide.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.juanp.tourappguide.Adapter.CustomAdapter;
import com.example.juanp.tourappguide.Objects.Items;
import com.example.juanp.tourappguide.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Places_of_interest extends Fragment {

    //Declaration of the arraylist with the places of interest
    private ArrayList<Items> listInterestPlaces;
    //Declaration of the recyclerview
    private RecyclerView recyclerInterestPlaces;

    public Places_of_interest() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Create the view and inflate it
        View view = inflater.inflate(R.layout.fragment_contents, container, false);

        //Method to add the items to the arraylist
        listInterestPlaces();
        //Method to create the recyclerview
        buildRecyclerView(view);

        return view;
    }

    //List method
    private void listInterestPlaces() {

        listInterestPlaces = new ArrayList<>();

        //Create items
        listInterestPlaces.add(new Items(getString(R.string.places_of_interest_hercules_name), getString(R.string.places_of_interest_hercules_location), getString(R.string.places_of_interest_hercules_info), getString(R.string.places_of_interest_hercules_coordinates), R.drawable.places_hercules));
        listInterestPlaces.add(new Items(getString(R.string.places_of_interest_anton_castle_name), getString(R.string.places_of_interest_anton_castle_location), getString(R.string.places_of_interest_anton_castle_info), getString(R.string.places_of_interest_anton_castle_coordinates), R.drawable.places_antoncastle));
        listInterestPlaces.add(new Items(getString(R.string.places_of_interest_millenium_name), getString(R.string.places_of_interest_millenium_location), getString(R.string.places_of_interest_millenium_info), getString(R.string.places_of_interest_millenium_coordinates), R.drawable.places_millenium));
        listInterestPlaces.add(new Items(getString(R.string.places_of_interest_sanpeter_name), getString(R.string.places_of_interest_sanpeter_location), getString(R.string.places_of_interest_sanpeter_info), getString(R.string.places_of_interest_sanpeter_coordinates), R.drawable.places_sanpeter));
    }

    //Build recyclerview method
    private void buildRecyclerView(View view) {

        //Set the view id and the layout manager
        recyclerInterestPlaces = view.findViewById(R.id.recyclerContents);
        recyclerInterestPlaces.setLayoutManager(new LinearLayoutManager(getContext()));

        //Create and set the adapter
        CustomAdapter interestPlacesAdapter = new CustomAdapter(listInterestPlaces);
        recyclerInterestPlaces.setAdapter(interestPlacesAdapter);

        //Clicklistener
        interestPlacesAdapter.setOnItemClickListener(new CustomAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                //Intent to open google maps location
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + listInterestPlaces.get(position).getCoordinates());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
    }
}
