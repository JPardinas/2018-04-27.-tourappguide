package com.example.juanp.tourappguide.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.juanp.tourappguide.Adapter.CustomAdapter;
import com.example.juanp.tourappguide.Objects.Items;
import com.example.juanp.tourappguide.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Beaches extends Fragment {

    //Declaration of the arraylist
    private ArrayList<Items> listBeaches;
    //Declaration of the recyclerview
    private RecyclerView recyclerBeaches;

    public Beaches() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Create the view and inflate it
        View view = inflater.inflate(R.layout.fragment_contents, container, false);

        //Method to add the items to the arraylist
        listBeaches();
        //Method to create the recyclerview
        buildRecyclerView(view);

        return view;
    }

    //List method
    private void listBeaches() {

        listBeaches = new ArrayList<>();

        //Create items
        listBeaches.add(new Items(getString(R.string.beach_orzan_name), getString(R.string.beach_orzan_location), getString(R.string.beach_orzan_info), getString(R.string.beach_orzan_coordinates), R.drawable.beach_orzan));
        listBeaches.add(new Items(getString(R.string.beach_matadero_name), getString(R.string.beach_matadero_location), getString(R.string.beach_matadero_info), getString(R.string.beach_matadero_coordinates), R.drawable.beach_matadero));
        listBeaches.add(new Items(getString(R.string.beach_margaritas_name), getString(R.string.beach_margaritas_location), getString(R.string.beach_margaritas_info), getString(R.string.beach_margaritas_coordinates), R.drawable.beach_margaritas));
        listBeaches.add(new Items(getString(R.string.beach_canabal_name), getString(R.string.beach_canabal_location), getString(R.string.beach_canabal_info), getString(R.string.beach_canabal_coordinates), R.drawable.beach_canabal));
    }

    //Build recyclerview method
    private void buildRecyclerView(View view) {

        //Set the view id and the layout manager
        recyclerBeaches = view.findViewById(R.id.recyclerContents);
        recyclerBeaches.setLayoutManager(new LinearLayoutManager(getContext()));

        //Create and set the adapter
        CustomAdapter beachesAdapter = new CustomAdapter(listBeaches);
        recyclerBeaches.setAdapter(beachesAdapter);

        //Clicklistener
        beachesAdapter.setOnItemClickListener(new CustomAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                //Intent to open google maps location
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + listBeaches.get(position).getCoordinates());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
    }
}
