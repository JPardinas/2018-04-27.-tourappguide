package com.example.juanp.tourappguide.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.juanp.tourappguide.Objects.Items;
import com.example.juanp.tourappguide.R;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolderList> {

    //Arraylist and ClickHandler initialize
    private ArrayList<Items> listAllItemss;
    private OnItemClickListener mListener;

    //The interface that receives onClick messages.
    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    //Set listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    public static class ViewHolderList extends RecyclerView.ViewHolder {

        //Initialize variables
        private TextView textName;
        private TextView textLocation;
        private TextView textInfo;
        private ImageView imgItem;

        public ViewHolderList(View itemView, final OnItemClickListener listener) {

            super(itemView);
            imgItem = itemView.findViewById(R.id.imgItem);
            textName = itemView.findViewById(R.id.textName);
            textLocation = itemView.findViewById(R.id.textLocation);
            textInfo = itemView.findViewById(R.id.textInfo);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    //Constructor
    public CustomAdapter(ArrayList<Items> listAllItemss) {
        this.listAllItemss = listAllItemss;
    }

    @NonNull
    @Override
    public ViewHolderList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            //Set LayoutInflater
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_list, parent, false);
            ViewHolderList evh = new ViewHolderList(v, mListener);
            return evh;


    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderList holder, int position) {

        //Get the position
        Items item = listAllItemss.get(position);
        //Set the info
        holder.textName.setText(item.getName());
        holder.textLocation.setText(item.getLocation());
        holder.textInfo.setText(item.getInformation());
        holder.imgItem.setImageResource(item.getImgItem());
    }

    @Override
    public int getItemCount() {

        //Get number of items
        return listAllItemss.size();
    }

    //Filter
    public void filterList(ArrayList<Items> filteredList){

        listAllItemss = new ArrayList<>();
        listAllItemss.addAll(filteredList);
        notifyDataSetChanged();

    }
}
